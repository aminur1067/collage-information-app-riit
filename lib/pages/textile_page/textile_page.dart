import 'package:flutter/material.dart';

import '../../constants.dart';

class TextilePage extends StatefulWidget {
  const TextilePage({Key? key}) : super(key: key);

  @override
  State<TextilePage> createState() => _TextilePageState();
}

class _TextilePageState extends State<TextilePage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(

        appBar: AppBar(
          shadowColor: Colors.white,
          title: Text('Text'),
        ),
        body: Padding(
          padding: const EdgeInsets.only(right: 5,top: 15),

          child: Column(

            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: MaterialButton(
                      onPressed: (){},
                      child: Container(

                        // padding: EdgeInsets.all(15),
                        height: 140,
                        width: 150,
                        child: Column(
                          children: [
                            Container(

                              decoration: BoxDecoration(
                                color: Colors.orange,
                                borderRadius: BorderRadius.all(Radius.circular(30)),
                              ),
                              child: ClipRRect(
                                borderRadius: BorderRadius.only(topLeft: Radius.circular(20),
                                    topRight: Radius.circular(20)),
                                child: Image.network(
                                  'https://www.riitpolybd.com/admin-website/images/1656400571Robi.jpg',
                                  fit: BoxFit.fill,
                                  height: 100,
                                  width:400,



                                ),
                              ),

                            ),
                            // SizedBox(height: 4,),
                            // Row(
                            //   children: [
                            //     Text('Most. Hosneara Parvin',style: TextStyle(fontSize: 12),)
                            //   ],
                            // ),
                            Column(
                              children: [
                                Container(
                                  height: 40,
                                  width: 450,
                                  decoration: BoxDecoration(
                                    color: Colors.orange,
                                    borderRadius: BorderRadius.only(bottomRight:Radius.circular(20),
                                        bottomLeft: Radius.circular(20)),
                                  ),
                                  child: Column(
                                    children: [
                                      Text('      	Md. Rabiul Islam    ',
                                          style: TextStyle(fontSize: 12)),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          Center(child:ProfileDetails)
                                        ],
                                      )
                                    ],

                                  ),


                                ),
                              ],
                            ),

                          ],
                        ),
                        decoration: BoxDecoration(
                          color: Colors.grey,
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(30),
                              topLeft: Radius.circular(30),
                              bottomLeft: Radius.circular(20),
                              bottomRight: Radius.circular(20)
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: 1,),

                  Expanded(
                    child: MaterialButton(
                      onPressed: (){},
                      child: Container(

                        // padding: EdgeInsets.all(15),
                        height: 140,
                        width: 150,
                        child: Column(
                          children: [
                            Container(

                              decoration: BoxDecoration(
                                color: Colors.orange,
                                borderRadius: BorderRadius.all(Radius.circular(30)),
                              ),

                              child: ClipRRect(
                                borderRadius: BorderRadius.only(topLeft: Radius.circular(20),
                                    topRight: Radius.circular(20)),

                                child: Image.network(
                                  'https://www.riitpolybd.com/admin-website/images/1656400571Robi.jpg',
                                  fit: BoxFit.fill,
                                  height: 100,
                                  width:400,



                                ),
                              ),
                            ),



                            // SizedBox(height: 4,),
                            // Row(
                            //   children: [
                            //     Text('Most. Hosneara Parvin',style: TextStyle(fontSize: 12),)
                            //   ],
                            // ),
                            Column(
                              children: [
                                Container(
                                  height: 40,
                                  width: 450,
                                  decoration: BoxDecoration(
                                    color: Colors.orange,
                                    borderRadius: BorderRadius.only(bottomRight:Radius.circular(20),
                                        bottomLeft: Radius.circular(20)),
                                  ),
                                  child: Column(
                                    children: [
                                      Text('      	Md. Rabiul Islam    ',
                                          style: TextStyle(fontSize: 12)),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          Center(child:ProfileDetails)
                                        ],
                                      )
                                    ],

                                  ),


                                ),
                              ],
                            ),

                          ],
                        ),
                        decoration: BoxDecoration(
                          color: Colors.grey,
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(30),
                              topLeft: Radius.circular(30),
                              bottomLeft: Radius.circular(20),
                              bottomRight: Radius.circular(20)
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: MaterialButton(
                      onPressed: (){},
                      child: Container(

                        // padding: EdgeInsets.all(15),
                        height: 140,
                        width: 150,
                        child: Column(
                          children: [
                            Container(

                              decoration: BoxDecoration(
                                color: Colors.orange,
                                borderRadius: BorderRadius.all(Radius.circular(30)),
                              ),
                              child: ClipRRect(
                                borderRadius: BorderRadius.only(topLeft: Radius.circular(20),
                                    topRight: Radius.circular(20)),
                                child: Image.network(
                                  'https://www.riitpolybd.com/admin-website/images/1656400571Robi.jpg',
                                  fit: BoxFit.fill,
                                  height: 100,
                                  width:400,



                                ),
                              ),

                            ),
                            // SizedBox(height: 4,),
                            // Row(
                            //   children: [
                            //     Text('Most. Hosneara Parvin',style: TextStyle(fontSize: 12),)
                            //   ],
                            // ),
                            Column(
                              children: [
                                Container(
                                  height: 40,
                                  width: 450,
                                  decoration: BoxDecoration(
                                    color: Colors.orange,
                                    borderRadius: BorderRadius.only(bottomRight:Radius.circular(20),
                                        bottomLeft: Radius.circular(20)),
                                  ),
                                  child: Column(
                                    children: [
                                      Text('      	Md. Rabiul Islam    ',
                                          style: TextStyle(fontSize: 12)),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          Center(child:ProfileDetails)
                                        ],
                                      )
                                    ],

                                  ),


                                ),
                              ],
                            ),

                          ],
                        ),
                        decoration: BoxDecoration(
                          color: Colors.grey,
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(30),
                              topLeft: Radius.circular(30),
                              bottomLeft: Radius.circular(20),
                              bottomRight: Radius.circular(20)
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 2,
                  ),
                  Expanded(
                    child: MaterialButton(
                      onPressed: (){},
                      child: Container(

                        // padding: EdgeInsets.all(15),
                        height: 140,
                        width: 150,
                        child: Column(
                          children: [
                            Container(

                              decoration: BoxDecoration(
                                color: Colors.orange,
                                borderRadius: BorderRadius.all(Radius.circular(30)),
                              ),
                              child: ClipRRect(
                                borderRadius: BorderRadius.only(topLeft: Radius.circular(20),
                                    topRight: Radius.circular(20)),
                                child: Image.network(
                                  'https://www.riitpolybd.com/admin-website/images/1656400571Robi.jpg',
                                  fit: BoxFit.fill,
                                  height: 100,
                                  width:400,



                                ),
                              ),

                            ),
                            // SizedBox(height: 4,),
                            // Row(
                            //   children: [
                            //     Text('Most. Hosneara Parvin',style: TextStyle(fontSize: 12),)
                            //   ],
                            // ),
                            Column(
                              children: [
                                Container(
                                  height: 40,
                                  width: 450,
                                  decoration: BoxDecoration(
                                    color: Colors.orange,
                                    borderRadius: BorderRadius.only(bottomRight:Radius.circular(20),
                                        bottomLeft: Radius.circular(20)),
                                  ),
                                  child: Column(
                                    children: [
                                      Text('      	Md. Rabiul Islam    ',
                                          style: TextStyle(fontSize: 12)),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          Center(child:ProfileDetails)
                                        ],
                                      )
                                    ],

                                  ),


                                ),
                              ],
                            ),

                          ],
                        ),
                        decoration: BoxDecoration(
                          color: Colors.grey,
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(30),
                              topLeft: Radius.circular(30),
                              bottomLeft: Radius.circular(20),
                              bottomRight: Radius.circular(20)
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: MaterialButton(
                      onPressed: (){},
                      child: Container(

                        // padding: EdgeInsets.all(15),
                        height: 140,
                        width: 150,
                        child: Column(
                          children: [
                            Container(

                              decoration: BoxDecoration(
                                color: Colors.orange,
                                borderRadius: BorderRadius.all(Radius.circular(30)),
                              ),
                              child: ClipRRect(
                                borderRadius: BorderRadius.only(topLeft: Radius.circular(20),
                                    topRight: Radius.circular(20)),
                                child: Image.network(
                                  'https://www.riitpolybd.com/admin-website/images/1656400571Robi.jpg',
                                  fit: BoxFit.fill,
                                  height: 100,
                                  width:400,



                                ),
                              ),

                            ),
                            // SizedBox(height: 4,),
                            // Row(
                            //   children: [
                            //     Text('Most. Hosneara Parvin',style: TextStyle(fontSize: 12),)
                            //   ],
                            // ),
                            Column(
                              children: [
                                Container(
                                  height: 40,
                                  width: 450,
                                  decoration: BoxDecoration(
                                    color: Colors.orange,
                                    borderRadius: BorderRadius.only(bottomRight:Radius.circular(20),
                                        bottomLeft: Radius.circular(20)),
                                  ),
                                  child: Column(
                                    children: [
                                      Text('      	Md. Rabiul Islam    ',
                                          style: TextStyle(fontSize: 12)),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          Center(child:ProfileDetails)
                                        ],
                                      )
                                    ],

                                  ),


                                ),
                              ],
                            ),

                          ],
                        ),
                        decoration: BoxDecoration(
                          color: Colors.grey,
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(30),
                              topLeft: Radius.circular(30),
                              bottomLeft: Radius.circular(20),
                              bottomRight: Radius.circular(20)
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 2,
                  ),
                  Expanded(
                    child: MaterialButton(
                      onPressed: (){},
                      child: Container(

                        // padding: EdgeInsets.all(15),
                        height: 140,
                        width: 150,
                        child: Column(
                          children: [
                            Container(

                              decoration: BoxDecoration(
                                color: Colors.orange,
                                borderRadius: BorderRadius.all(Radius.circular(30)),
                              ),
                              child: ClipRRect(
                                borderRadius: BorderRadius.only(topLeft: Radius.circular(20),
                                    topRight: Radius.circular(20)),
                                child: Image.network(
                                  'https://www.riitpolybd.com/admin-website/images/1656400571Robi.jpg',
                                  fit: BoxFit.fill,
                                  height: 100,
                                  width:400,



                                ),
                              ),

                            ),
                            // SizedBox(height: 4,),
                            // Row(
                            //   children: [
                            //     Text('Most. Hosneara Parvin',style: TextStyle(fontSize: 12),)
                            //   ],
                            // ),
                            Column(
                              children: [
                                Container(
                                  height: 40,
                                  width: 450,
                                  decoration: BoxDecoration(
                                    color: Colors.orange,
                                    borderRadius: BorderRadius.only(bottomRight:Radius.circular(20),
                                        bottomLeft: Radius.circular(20)),
                                  ),
                                  child: Column(
                                    children: [
                                      Text('      	Md. Rabiul Islam    ',
                                          style: TextStyle(fontSize: 12)),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          Center(child:ProfileDetails)
                                        ],
                                      )
                                    ],

                                  ),


                                ),
                              ],
                            ),

                          ],
                        ),
                        decoration: BoxDecoration(
                          color: Colors.grey,
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(30),
                              topLeft: Radius.circular(30),
                              bottomLeft: Radius.circular(20),
                              bottomRight: Radius.circular(20)
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
