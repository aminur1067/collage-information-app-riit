import 'package:flutter/material.dart';
import 'package:flutter_phone_direct_caller/flutter_phone_direct_caller.dart';
//
// import 'package:flutter_phone_direct_caller/flutter_phone_direct_caller.dart';
import 'package:url_launcher/url_launcher.dart';
class EmdadhulPages extends StatefulWidget {
  const EmdadhulPages({Key? key}) : super(key: key);

  @override
  State<EmdadhulPages> createState() => _EmdadhulPagesState();
}

class _EmdadhulPagesState extends State<EmdadhulPages> {
  String number='01750403009';
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      backgroundColor: Colors.white,
      // appBar: AppBar(
      //   title: Text('Profile'),
      // ),

      body: Stack(
        children: [
            Column(
            children: [
              Container(
                child:ClipRRect(
                    borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30),bottomRight: Radius.circular(30)),
                    child: Image.network('https://www.riitpolybd.com/admin-website/images/1656397665Amdath.jpg',fit: BoxFit.fill,)),
                decoration: BoxDecoration(
                    color: Colors.grey.shade300,
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(15), topLeft: Radius.circular(15))
                ),

                height: 200,
                width: 500,
              ),
              Expanded(
                child: Container(
                  color: Colors.grey.shade300,

                  child: Column(
                    children: [
                      Container(
                          padding: EdgeInsets.only(top: 20),
                          child: Text('Emdadhul Haque	',style: TextStyle(color: Colors.blue),)
                      ),

                      SizedBox(height: 10,),
                      Container(

                          child: Text('Instructor	',style: TextStyle(color: Colors.blue),)
                      ),
                      SizedBox(height: 10,),
                      Container(

                          child: Text('Head off Department Computer	',style: TextStyle(color: Colors.blue),)
                      ),
                      SizedBox(height: 5,),
                      Container(

                          child: Text('Rangpur Ideal Institute of Technology	',style: TextStyle(color: Colors.blue),)
                      ),
                      SizedBox(height: 15,),
                      Row(
                        children: [
                          Container(
                              width: 250,
                              child: SelectableText('   Phone namber: 01750403009',style: TextStyle(color: Colors.blue),)
                          ),
                          ElevatedButton(onPressed: () async{
                            // await FlutterPhoneDirectCaller.callNumber(number);

                            final Uri Url=Uri(
                              scheme: 'tel',
                              path: '01750403009'
                            );
                            if(await canLaunchUrl(Url)){
                              await launchUrl(Url);
                            }else{
                              print('cannot');
                            }

                          }, child:  Container( color:Colors.orangeAccent,child: Icon(Icons.call)))

                        ],
                      )
                    ],
                  ),
                  height: 300,
                  width: 500,

                ),
              )
            ],
          ),
        ],
      ),
    ));
  }
}
