import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NationalPage extends StatefulWidget {
  const NationalPage({Key? key}) : super(key: key);

  @override
  State<NationalPage> createState() => _NationalPageState();
}

class _NationalPageState extends State<NationalPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(child: Scaffold(
      appBar: AppBar(),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Center(
            child: Text('333 National Info Service\n999 Emergency Service\n106 dudok',style: TextStyle(
              backgroundColor: Colors.yellowAccent,color: Colors.red
            ),),
          )
        ],
      ),
    ));
  }
}
