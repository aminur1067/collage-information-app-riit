import 'package:flutter/material.dart';

import '../../constants.dart';

class JobCornerRiit extends StatefulWidget {
  const JobCornerRiit({Key? key}) : super(key: key);

  @override
  State<JobCornerRiit> createState() => _JobCornerRiitState();
}

class _JobCornerRiitState extends State<JobCornerRiit> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('amarCempass'),
          actions: [

          ],
        ),
        body:  GridView.extent(
          primary: false,
          padding: const EdgeInsets.all(16),
          crossAxisSpacing: 10,
          mainAxisSpacing: 10,
          maxCrossAxisExtent: 200.0,
          children: <Widget>[
            MaterialButton(onPressed: (){},
              padding: const EdgeInsets.all(8),
              child: Column(
                children: [
                  const Text('Job Notice', style: TextStyle(fontSize: 20)),
                  Row(
                    children: [
                      Image.network('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRzq3LFLHltkWr7VN0mC9pmd8STVu6g-njadg&usqp=CAU',height: 100,width: 140,)
                    ],
                  ),
                  details

                ],
              ),
              color: Colors.grey.shade100,
            ),
            MaterialButton(onPressed: (){},
              padding: const EdgeInsets.all(8),
              child: Column(
                children: [
                  const Text('Job Result', style: TextStyle(fontSize: 20)),
                  Row(
                    children: [
                      Image.network('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRzq3LFLHltkWr7VN0mC9pmd8STVu6g-njadg&usqp=CAU',height: 100,width: 140,)
                    ],
                  ),
                  details

                ],
              ),
              color: Colors.grey.shade100,
            ),
            MaterialButton(onPressed: (){},
              padding: const EdgeInsets.all(8),
              child: Column(
                children: [
                  const Text('Aplication Form', style: TextStyle(fontSize: 20)),
                  Row(
                    children: [
                      Image.network('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRzq3LFLHltkWr7VN0mC9pmd8STVu6g-njadg&usqp=CAU',height: 100,width: 140,)
                    ],
                  ),
                  details

                ],
              ),
              color: Colors.grey.shade100,
            ),
            MaterialButton(onPressed: (){},
              padding: const EdgeInsets.all(8),
              child: Column(
                children: [
                  const Text('Find job', style: TextStyle(fontSize: 20)),
                  Row(
                    children: [
                      Image.network('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRzq3LFLHltkWr7VN0mC9pmd8STVu6g-njadg&usqp=CAU',height: 100,width: 140,)
                    ],
                  ),
                  details

                ],
              ),
              color: Colors.grey.shade100,
            ),
          ],
        ),
      ),
    );
  }
}
