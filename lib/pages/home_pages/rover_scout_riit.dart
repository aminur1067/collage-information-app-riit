

import 'package:flutter/material.dart';

import '../../constants.dart';

class RoverScoutRiit extends StatefulWidget {
  const RoverScoutRiit({Key? key}) : super(key: key);

  @override
  State<RoverScoutRiit> createState() => _RoverScoutRiitState();
}

class _RoverScoutRiitState extends State<RoverScoutRiit> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('amarCempass'),
          actions: [

          ],
        ),
        body:  GridView.extent(
          primary: false,
          padding: const EdgeInsets.all(16),
          crossAxisSpacing: 10,
          mainAxisSpacing: 10,
          maxCrossAxisExtent: 200.0,
          children: <Widget>[
            MaterialButton(onPressed: (){},
              padding: const EdgeInsets.all(8),
              child: Column(
                children: [
                  const Text('National Activites', style: TextStyle(fontSize: 10)),
                  Row(
                    children: [
                      Image.network('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSm6x0QeEAMHqcRcAzEJWITCEWffamD6nWsfA&usqp=CAU',height: 100,width: 140,)
                    ],
                  ),
                  details
                ],
              ),
              color: Colors.grey.shade100,
            ),
            MaterialButton(onPressed: (){},
              padding: const EdgeInsets.all(8),
              child: Column(
                children: [
                  const Text('Events  (National)  ', style: TextStyle(fontSize: 10)),
                  Row(
                    children: [
                      Image.network('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSm6x0QeEAMHqcRcAzEJWITCEWffamD6nWsfA&usqp=CAU',height: 100,width: 140,)
                    ],
                  ),
                  details
                ],
              ),
              color: Colors.grey.shade100,
            ),
            MaterialButton(onPressed: (){},
              padding: const EdgeInsets.all(8),
              child: Column(
                children: [
                  const Text('Award', style: TextStyle(fontSize: 10)),
                  Row(
                    children: [
                      Image.network('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSm6x0QeEAMHqcRcAzEJWITCEWffamD6nWsfA&usqp=CAU',height: 100,width: 140,)
                    ],
                  ),
                  details
                ],
              ),
              color: Colors.grey.shade100,
            ),
            MaterialButton(onPressed: (){},
              padding: const EdgeInsets.all(8),
              child: Column(
                children: [
                  const Text('Result', style: TextStyle(fontSize: 10)),
                  Row(
                    children: [
                      Image.network('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSm6x0QeEAMHqcRcAzEJWITCEWffamD6nWsfA&usqp=CAU',height: 100,width: 140,)
                    ],
                  ),
                  details
                ],
              ),
              color: Colors.grey.shade100,
            ),
          ],
        ),
      ),
    );
  }
}
