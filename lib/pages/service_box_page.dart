import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ServiceBoxPage extends StatefulWidget {
  const ServiceBoxPage({Key? key}) : super(key: key);

  @override
  State<ServiceBoxPage> createState() => _ServiceBoxPageState();
}

class _ServiceBoxPageState extends State<ServiceBoxPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: const [
          Center(
              child: Text(
            'তথ্য সেবা নম্বর : 01717-100467',
            style: TextStyle(backgroundColor: Colors.yellowAccent),
          ))
        ],
      ),
    ));
  }
}
