import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';


import '../../constants.dart';
import '../civil_page/civil_teachers_page.dart';
import '../computer_techers_page/computer_teacher.dart';
import '../electrical_page/electrical_teacher_page.dart';
import '../electronics_page/electronics_page.dart';
import '../mechanical_page/mechaniacal_page.dart';
import '../textile_page/textile_page.dart';

class TeacherOffical extends StatefulWidget {
  const TeacherOffical({Key? key}) : super(key: key);

  @override
  State<TeacherOffical> createState() => _TeacherOfficalState();
}

class _TeacherOfficalState extends State<TeacherOffical> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(child: Scaffold(
      appBar: AppBar(
        title: Text('Teachers & offical'),
      ),
      body: Container(


        child: GridView.extent(

          primary: false,
          padding: const EdgeInsets.all(16),
          crossAxisSpacing: 10,
          mainAxisSpacing: 10,
          maxCrossAxisExtent: 200.0,
          children: <Widget>[
            MaterialButton(
              onPressed: () {},
              padding: const EdgeInsets.all(8),
              color: Colors.grey.shade100,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                    child: Text('Director ',
                        style: TextStyle(fontSize: 20)),
                  ),
                  Row(
                    children: [
                      Image.network(
                        'https://www.riitpolybd.com/admin-website/images/15833750701574226904Hena%20Sir.jpg',
                        width: 140,
                        height: 100,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        '             Abu Hena Md.Mostofa Kamal',
                        style: TextStyle(
                            fontSize: 7,
                            color: Colors.blue.withOpacity(1.0)),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      Text(
                        '            Rangpur Ideal Institute of Technology',
                        style: TextStyle(fontSize: 6),
                      )
                    ],
                  )
                ],
              ),
            ),
            MaterialButton(
              onPressed: () {},
              padding: const EdgeInsets.all(8),
              color: Colors.grey.shade100,
              child: Column(
                children: [
                  Text('  Head Of the Institute  ',
                      style: TextStyle(fontSize: 14)),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    children: [
                      Image.network(
                        'https://www.riitpolybd.com/admin-website/images/15833748171563185904Principal.jpg',
                        width: 140,
                        height: 100,
                      )
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        'Md. Shakinur Alam',
                        style: TextStyle(
                            fontSize: 8,
                            color: Colors.blue.withOpacity(1.0)),
                      )
                    ],
                  ),
                  Row(
                    children: [
                      Text(
                        '            Rangpur Ideal Institute of Technology',
                        style: TextStyle(fontSize: 6),
                      )
                    ],
                  )
                ],
              ),
            ),
            MaterialButton(
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context)=>ComputerTeacherPage()));
              },
              padding: const EdgeInsets.all(8),
              color: Colors.grey.shade100,
              child: Column(
                children: [
                  Text('  Computer  ',
                      style: TextStyle(fontSize: 14)),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    children: [
                      Image.network(
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS9gw3AUi-PX9IfUaNCsvJzCuJT1MnwFTiOgw&usqp=CAU',
                        width: 140,
                        height: 100,
                      )
                    ],
                  ),
                  SizedBox(
                    height: 1,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      details
                    ],
                  ),
                  Row(
                    children: [
                      Text(
                        '            Rangpur Ideal Institute of Technology',
                        style: TextStyle(fontSize: 6),
                      )
                    ],
                  )
                ],
              ),
            ),
            MaterialButton(
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context)=>ElectricalTeacherPage()));
              },
              padding: const EdgeInsets.all(8),
              color: Colors.grey.shade100,
              child: Column(
                children: [
                  Text('  Electrical  ',
                      style: TextStyle(fontSize: 14)),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    children: [
                      CachedNetworkImage(
                    imageUrl:

                          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRZyBdcJqKDepDygYK0WdaoVGNlHNQewwOpzA&usqp=CAU',
                          width: 140,
                          height: 100,

                      )
                    ],
                  ),
                  SizedBox(
                    height: 1,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      details
                    ],
                  ),
                  Row(
                    children: [
                      Text(
                        '            Rangpur Ideal Institute of Technology',
                        style: TextStyle(fontSize: 6),
                      )
                    ],
                  )
                ],
              ),
            ),
            MaterialButton(
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context)=>CivilTeacherPage()));
              },
              padding: const EdgeInsets.all(8),
              color: Colors.grey.shade100,
              child: Column(
                children: [
                  Text('  Civil  ',
                      style: TextStyle(fontSize: 14)),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    children: [
                      Image.network(
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSxMv4THBLDicQKVXP7FkJG1DcAoiT94VJWWA&usqp=CAU',
                        width: 140,
                        height: 100,
                      )
                    ],
                  ),
                  SizedBox(
                    height: 1,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      details
                    ],
                  ),
                  Row(
                    children: [
                      Text(
                        '            Rangpur Ideal Institute of Technology',
                        style: TextStyle(fontSize: 6),
                      )
                    ],
                  )
                ],
              ),
            ),
            MaterialButton(
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context)=>ElectrnicsPage()));
              },
              padding: const EdgeInsets.all(8),
              color: Colors.grey.shade100,
              child: Column(
                children: [
                  Text('  Mechanical  ',
                      style: TextStyle(fontSize: 14)),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    children: [
                      Image.network(
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ8cHhY7jbtVnBjXcik3ye3Wv7jwyKQVkuRZQ&usqp=CAU',
                        width: 140,
                        height: 100,
                      )
                    ],
                  ),
                  SizedBox(
                    height: 1,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      details
                    ],
                  ),
                  Row(
                    children: [
                      Text(
                        '            Rangpur Ideal Institute of Technology',
                        style: TextStyle(fontSize: 6),
                      )
                    ],
                  )
                ],
              ),
            ),
            MaterialButton(
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context)=>MechanicalPage()));
              },
              padding: const EdgeInsets.all(8),
              color: Colors.grey.shade100,
              child: Column(
                children: [
                  Text('  ELectronics  ',
                      style: TextStyle(fontSize: 14)),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    children: [
                      Image.network(
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRYxlKUIgMa-IOyXEtc6Wd8usUy6nf_oFUgiw&usqp=CAU',
                        width: 140,
                        height: 100,
                      )
                    ],
                  ),
                  SizedBox(
                    height: 1,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      details
                    ],
                  ),
                  Row(
                    children: [
                      Text(
                        '            Rangpur Ideal Institute of Technology',
                        style: TextStyle(fontSize: 6),
                      )
                    ],
                  )
                ],
              ),
            ),
            MaterialButton(
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context)=>TextilePage()));
              },
              padding: const EdgeInsets.all(8),
              color: Colors.grey.shade100,
              child: Column(
                children: [
                  Text('  Textile  ',
                      style: TextStyle(fontSize: 14)),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    children: [
                      Image.network(
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTth3iPPkse7sbSch2QsLpHwjyKtNJKTtYcLQ&usqp=CAU',
                        width: 140,
                        height: 100,
                      )
                    ],
                  ),
                  SizedBox(
                    height: 1,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      details
                    ],
                  ),
                  Row(
                    children: [
                      Text(
                        '            Rangpur Ideal Institute of Technology',
                        style: TextStyle(fontSize: 6),
                      )
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    ));
  }
}
