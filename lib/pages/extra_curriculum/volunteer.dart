import 'package:flutter/material.dart';

class VolunteerPage extends StatefulWidget {
  const VolunteerPage({Key? key}) : super(key: key);

  @override
  State<VolunteerPage> createState() => _VolunteerPageState();
}

class _VolunteerPageState extends State<VolunteerPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(child: Scaffold(
      appBar: AppBar(),
      body: Container(
        child: GridView.extent(
          primary: false,
          padding: const EdgeInsets.all(16),
          crossAxisSpacing: 10,
          mainAxisSpacing: 10,
          maxCrossAxisExtent: 200.0,
          children: <Widget>[
            MaterialButton(
              onPressed: () {
              },
              padding: const EdgeInsets.all(8),
              child: Column(
                children: [
                  const Text('Volunteer', style: TextStyle(fontSize: 20)),
                  Row(
                    children: [
                      Image.network(
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSYduUWo5cK8J43Z_-2kSA4ORQ_bALvBRBA_w&usqp=CAU',
                        height: 95,
                        width: 130,
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Column(
                    children: [
                      Text(
                        'details',
                        style: TextStyle(fontSize: 10, color: Colors.blue),
                      )
                    ],
                  )
                ],
              ),
              color: Colors.grey.shade100,
            ),
            MaterialButton(
              onPressed: () {
              },
              padding: const EdgeInsets.all(8),
              child: Column(
                children: [
                  const Text('Arts & Culture ', style: TextStyle(fontSize: 10)),
                  Row(
                    children: [
                      Image.network(
                        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcScIGHI2kfUTt3uWjw_NNs2JWS5RgQQarfwmw&usqp=CAU',
                        height: 95,
                        width: 130,
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Column(
                    children: [
                      Text(
                        'details',
                        style: TextStyle(fontSize: 10, color: Colors.blue),
                      )
                    ],
                  )
                ],
              ),
              color: Colors.grey.shade100,
            ),


          ],
        ),
      ),
    ));
  }
}
