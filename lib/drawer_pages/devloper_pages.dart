import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:page_transition/page_transition.dart';
import 'package:riit/drawer_pages/polash_page.dart';
import 'package:riit/drawer_pages/tahidul_page.dart';
import 'dart:math';

import 'aminur_profile.dart';
import 'naim_page.dart';

class DeveloperPages extends StatefulWidget {
  const DeveloperPages({Key? key}) : super(key: key);

  @override
  State<DeveloperPages> createState() => _DeveloperPagesState();
}

class _DeveloperPagesState extends State<DeveloperPages> {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      home: SafeArea(
          child: Scaffold(
        backgroundColor: Colors.grey.shade300,
        appBar: AppBar(
          title: Text('Developers'),
        ),
        body: Column(

          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.only(top: 10.0),
            ),
            FittedBox(
              child: Row( mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Padding(

                    padding: EdgeInsets.only(top: 20.0, left: 15),
                  ),
                  Container(
                    child: Center(
                      child: Text(
                          '                      we are flutter developers team \n'
                          '              Rangpur Ideal Institute Off Technology\n'
                          '                            session:18-19'),
                    ),
                    padding: EdgeInsets.only(top: 10),
                    width: 325,
                    height: 80,
                    color: Colors.lightBlue,
                  )
                ],
              ),
            ),
            SizedBox(
              height: 30,
            ),
            FittedBox(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,

                children: [


                  Padding(padding: EdgeInsets.all(13)),
                  InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          PageRouteBuilder(
                              transitionDuration: Duration(seconds: 3),
                              pageBuilder: (context, animation, animatintime) =>
                                  AminurPage(),
                              transitionsBuilder:
                                  (context, animation, animatintime, child) {
                                animation = CurvedAnimation(
                                    parent: animation,

                                    reverseCurve: Curves.easeInOutCubicEmphasized,
                                    curve: Curves.bounceInOut);
                                return ScaleTransition(
                                    scale: animation,
                                    alignment: Alignment.center,
                                    child: child);
                              }));
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.yellowAccent.shade700,
                          borderRadius: BorderRadius.all(Radius.circular(30))),
                      child: Column(
                        children: [
                          Container(
                            child: ClipRRect(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(25),
                                  topRight: Radius.circular(25)),
                              child: Image.network(
                                'https://scontent.fdac96-1.fna.fbcdn.net/v/t39.30808-6/328550892_1393227551480668_4742572841557205384_n.jpg?stp=dst-jpg_p843x403&_nc_cat=106&ccb=1-7&_nc_sid=8bfeb9&_nc_eui2=AeH7jtuLFq925cR-8bR1Yuipw33X0ftqDv3DfdfR-2oO_aIanNdNfoX51DG_7FNzw18jK-_cvbDfOD682T9dwdhP&_nc_ohc=fCLmVK4BTTYAX-9F-w6&_nc_zt=23&_nc_ht=scontent.fdac96-1.fna&oh=00_AfBnLmEOnj9hDk7daxookyYQqtcb9NxC_07v96eb6SMwKg&oe=63E4D1C2',
                                width: 150,
                                height: 120,
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 2,
                          ),
                          Container(
                            child: Text(
                              'MD AMINUR RAHMAN',
                              style: TextStyle(fontSize: 10),
                            ),
                          ),
                          SizedBox(
                            height: 2,
                          ),
                          Container(
                            child: Text(
                              ' Click For more details profile',
                              style: TextStyle(
                                  fontSize: 9, fontWeight: FontWeight.bold),
                            ),
                          )
                        ],
                      ),
                      width: 150,
                      height: 150,
                    ),
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.push(context,
                          PageRouteBuilder(
                          transitionDuration: Duration(seconds: 3),
                      pageBuilder: (context, animation, animatintime) => TahidulPage(),
                      transitionsBuilder: (context, animation, animatintime, child){
                      animation=CurvedAnimation(parent: animation, curve: Curves.easeInToLinear);
                      return ScaleTransition(scale: animation,alignment:Alignment.center,child:child


                      );

                      }));
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.yellowAccent.shade700,
                          borderRadius: BorderRadius.all(Radius.circular(30))),
                      child: Column(
                        children: [
                          Container(
                            child: ClipRRect(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(25),
                                  topRight: Radius.circular(25)),
                              child: Image.network(
                                'https://scontent.fdac96-1.fna.fbcdn.net/v/t1.6435-9/117769170_867896973736212_2949924993613681954_n.jpg?_nc_cat=106&ccb=1-7&_nc_sid=174925&_nc_eui2=AeHXrSR1Qbwez7nNge9lz88KJWJt79uvrRglYm3v26-tGLNesImDwDl-CCsoQVTHgjAp9duvdMDRUPP6RLxGe87p&_nc_ohc=edvTFXsx_B0AX_LYMJh&_nc_ht=scontent.fdac96-1.fna&oh=00_AfDXpA0WjxtAGUeBj2gDv6NhnisX0gKSkAJv4k0X1YXlZw&oe=6406B31F',
                                width: 150,
                                height: 120,
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 2,
                          ),
                          Container(
                            child: Text(
                              'MD TAUHIDUL ISLAM',
                              style: TextStyle(fontSize: 10),
                            ),
                          ),
                          SizedBox(
                            height: 2,
                          ),
                          Container(
                            child: Text(
                              ' Click For more details profile',
                              style: TextStyle(
                                  fontSize: 9, fontWeight: FontWeight.bold),
                            ),
                          )
                        ],
                      ),
                      width: 150,
                      height: 150,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(padding: EdgeInsets.all(13)),
                InkWell(
                  onTap: () {
                    Navigator.push(context,
                        PageRouteBuilder(
                            transitionDuration: Duration(seconds: 3),
                            pageBuilder: (context, animation, animatintime) => NaimPage(),
                            transitionsBuilder: (context, animation, animatintime, child){
                              animation=CurvedAnimation(parent: animation, curve: Curves.easeInToLinear);
                              return ScaleTransition(scale: animation,alignment:Alignment.center,child:child


                              );

                            }));
                  },
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.yellowAccent.shade700,
                        borderRadius: BorderRadius.all(Radius.circular(30))),
                    child: Column(
                      children: [
                        Container(
                          child: ClipRRect(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(25),
                                topRight: Radius.circular(25)),
                            child: Image.network(
                              'https://scontent.fdac96-1.fna.fbcdn.net/v/t39.30808-6/299420338_1136904793558549_6390555379185670664_n.jpg?stp=dst-jpg_p843x403&_nc_cat=103&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeG3jDQQywf_w1ob9M6OMGqh3sgcW8pJ657eyBxbyknrnv8xLmmKjw_erDsYrilY8SoygbgPOVQ-2QRNpiG9g6iu&_nc_ohc=2jPrai0lopQAX_Wsydx&_nc_zt=23&_nc_ht=scontent.fdac96-1.fna&oh=00_AfCdZl8FnTQmKlHmyL-ECM-C-9lDbVysPwOaM6ucuvvYsA&oe=63E3E451',
                              width: 150,
                              height: 120,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 2,
                        ),
                        Container(
                          child: Text(
                            'MD NAIM HOSEN NAIM',
                            style: TextStyle(fontSize: 10),
                          ),
                        ),
                        SizedBox(
                          height: 2,
                        ),
                        Container(
                          child: Text(
                            ' Click For more details profile',
                            style: TextStyle(
                                fontSize: 9, fontWeight: FontWeight.bold),
                          ),
                        )
                      ],
                    ),
                    width: 150,
                    height: 150,
                  ),
                ),
                SizedBox(
                  width: 15,
                ),
                InkWell(
                  onTap: () {
                    Navigator.push(context,
                        PageRouteBuilder(
                            transitionDuration: Duration(seconds: 3),
                            pageBuilder: (context, animation, animatintime) => PolashPage(),
                            transitionsBuilder: (context, animation, animatintime, child){
                              animation=CurvedAnimation(parent: animation, curve: Curves.easeInToLinear);
                              return ScaleTransition(scale: animation,alignment:Alignment.center,child:child


                              );

                            }));
                  },
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.yellowAccent.shade700,
                        borderRadius: BorderRadius.all(Radius.circular(30))),
                    child: Column(
                      children: [
                        Container(
                          child: ClipRRect(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(25),
                                topRight: Radius.circular(25)),
                            child: Image.network(
                              'https://scontent.fdac96-1.fna.fbcdn.net/v/t39.30808-6/288017973_1421916174946846_7024210241154818512_n.jpg?stp=c240.0.960.960a_dst-jpg_p960x960&_nc_cat=102&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeHUZ0WkpzPix6JVQ0wQKhzCDEeUEbAWZVAMR5QRsBZlUMCJ8j7EI6pW594UHsZbFoC6yp_3XKkI0XuzSoU93fw2&_nc_ohc=BYMYY_hsG7YAX-frcCo&_nc_zt=23&_nc_ht=scontent.fdac96-1.fna&oh=00_AfAyHwk2Ogfencq5yCKq0OZpyz66JXvyV8siBphzSWkQGw&oe=63E45147',
                              width: 150,
                              height: 120,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 2,
                        ),
                        Container(
                          child: Text(
                            'POLASH ROY',
                            style: TextStyle(fontSize: 10),
                          ),
                        ),
                        SizedBox(
                          height: 2,
                        ),
                        Container(
                          child: Text(
                            ' Click For more details profile',
                            style: TextStyle(
                                fontSize: 9, fontWeight: FontWeight.bold),
                          ),
                        )
                      ],
                    ),
                    width: 150,
                    height: 150,
                  ),
                ),
              ],
            )
          ],
        ),
      )),
    );
    SafeArea(
        child: Scaffold(
      backgroundColor: Colors.grey.shade300,
      appBar: AppBar(
        title: Text('Developers'),
      ),
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(top: 10.0),
          ),
          Row(
            children: [
              Padding(
                padding: EdgeInsets.only(top: 20.0, left: 10),
              ),
              Container(
                child: Center(
                  child: Text(
                      '                      we are flutter developers team \n'
                      '              Rangpur Ideal Institute Off Technology\n'
                      '                            session:18-19'),
                ),
                padding: EdgeInsets.only(top: 10),
                width: 340,
                height: 80,
                color: Colors.lightBlue,
              )
            ],
          ),
          SizedBox(
            height: 30,
          ),
          Row(
            children: [
              Padding(padding: EdgeInsets.all(13)),
              InkWell(
                onTap: () {
                  Get.to((context) => AminurPage(),
                      transition: Transition.downToUp,
                      duration: Duration(seconds: 5));
                },
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.yellowAccent.shade700,
                      borderRadius: BorderRadius.all(Radius.circular(30))),
                  child: Column(
                    children: [
                      Container(
                        child: ClipRRect(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(25),
                              topRight: Radius.circular(25)),
                          child: Image.network(
                            'https://scontent.fdac96-1.fna.fbcdn.net/v/t39.30808-6/328550892_1393227551480668_4742572841557205384_n.jpg?stp=dst-jpg_p843x403&_nc_cat=106&ccb=1-7&_nc_sid=8bfeb9&_nc_eui2=AeH7jtuLFq925cR-8bR1Yuipw33X0ftqDv3DfdfR-2oO_aIanNdNfoX51DG_7FNzw18jK-_cvbDfOD682T9dwdhP&_nc_ohc=fCLmVK4BTTYAX-9F-w6&_nc_zt=23&_nc_ht=scontent.fdac96-1.fna&oh=00_AfBnLmEOnj9hDk7daxookyYQqtcb9NxC_07v96eb6SMwKg&oe=63E4D1C2',
                            width: 150,
                            height: 120,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 2,
                      ),
                      Container(
                        child: Text(
                          'MD AMINUR RAHMAN',
                          style: TextStyle(fontSize: 10),
                        ),
                      ),
                      SizedBox(
                        height: 2,
                      ),
                      Container(
                        child: Text(
                          ' Click For more details profile',
                          style: TextStyle(
                              fontSize: 9, fontWeight: FontWeight.bold),
                        ),
                      )
                    ],
                  ),
                  width: 150,
                  height: 150,
                ),
              ),
              SizedBox(
                width: 10,
              ),
              InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => TahidulPage()));
                },
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.yellowAccent.shade700,
                      borderRadius: BorderRadius.all(Radius.circular(30))),
                  child: Column(
                    children: [
                      Container(
                        child: ClipRRect(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(25),
                              topRight: Radius.circular(25)),
                          child: Image.network(
                            'https://scontent.fdac96-1.fna.fbcdn.net/v/t1.6435-9/117769170_867896973736212_2949924993613681954_n.jpg?_nc_cat=106&ccb=1-7&_nc_sid=174925&_nc_eui2=AeHXrSR1Qbwez7nNge9lz88KJWJt79uvrRglYm3v26-tGLNesImDwDl-CCsoQVTHgjAp9duvdMDRUPP6RLxGe87p&_nc_ohc=edvTFXsx_B0AX_LYMJh&_nc_ht=scontent.fdac96-1.fna&oh=00_AfDXpA0WjxtAGUeBj2gDv6NhnisX0gKSkAJv4k0X1YXlZw&oe=6406B31F',
                            width: 150,
                            height: 120,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 2,
                      ),
                      Container(
                        child: Text(
                          'MD TAUHIDUL ISLAM',
                          style: TextStyle(fontSize: 10),
                        ),
                      ),
                      SizedBox(
                        height: 2,
                      ),
                      Container(
                        child: Text(
                          ' Click For more details profile',
                          style: TextStyle(
                              fontSize: 9, fontWeight: FontWeight.bold),
                        ),
                      )
                    ],
                  ),
                  width: 150,
                  height: 150,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Padding(padding: EdgeInsets.all(13)),
              InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => NaimPage()));
                },
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.yellowAccent.shade700,
                      borderRadius: BorderRadius.all(Radius.circular(30))),
                  child: Column(
                    children: [
                      Container(
                        child: ClipRRect(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(25),
                              topRight: Radius.circular(25)),
                          child: Image.network(
                            'https://scontent.fdac96-1.fna.fbcdn.net/v/t39.30808-6/299420338_1136904793558549_6390555379185670664_n.jpg?stp=dst-jpg_p843x403&_nc_cat=103&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeG3jDQQywf_w1ob9M6OMGqh3sgcW8pJ657eyBxbyknrnv8xLmmKjw_erDsYrilY8SoygbgPOVQ-2QRNpiG9g6iu&_nc_ohc=2jPrai0lopQAX_Wsydx&_nc_zt=23&_nc_ht=scontent.fdac96-1.fna&oh=00_AfCdZl8FnTQmKlHmyL-ECM-C-9lDbVysPwOaM6ucuvvYsA&oe=63E3E451',
                            width: 150,
                            height: 120,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 2,
                      ),
                      Container(
                        child: Text(
                          'MD NAIM HOSEN NAIM',
                          style: TextStyle(fontSize: 10),
                        ),
                      ),
                      SizedBox(
                        height: 2,
                      ),
                      Container(
                        child: Text(
                          ' Click For more details profile',
                          style: TextStyle(
                              fontSize: 9, fontWeight: FontWeight.bold),
                        ),
                      )
                    ],
                  ),
                  width: 150,
                  height: 150,
                ),
              ),
              SizedBox(
                width: 10,
              ),
              InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => PolashPage()));
                },
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.yellowAccent.shade700,
                      borderRadius: BorderRadius.all(Radius.circular(30))),
                  child: Column(
                    children: [
                      Container(
                        child: ClipRRect(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(25),
                              topRight: Radius.circular(25)),
                          child: Image.network(
                            'https://scontent.fdac96-1.fna.fbcdn.net/v/t39.30808-6/288017973_1421916174946846_7024210241154818512_n.jpg?stp=c240.0.960.960a_dst-jpg_p960x960&_nc_cat=102&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeHUZ0WkpzPix6JVQ0wQKhzCDEeUEbAWZVAMR5QRsBZlUMCJ8j7EI6pW594UHsZbFoC6yp_3XKkI0XuzSoU93fw2&_nc_ohc=BYMYY_hsG7YAX-frcCo&_nc_zt=23&_nc_ht=scontent.fdac96-1.fna&oh=00_AfAyHwk2Ogfencq5yCKq0OZpyz66JXvyV8siBphzSWkQGw&oe=63E45147',
                            width: 150,
                            height: 120,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 2,
                      ),
                      Container(
                        child: Text(
                          'POLASH ROY',
                          style: TextStyle(fontSize: 10),
                        ),
                      ),
                      SizedBox(
                        height: 2,
                      ),
                      Container(
                        child: Text(
                          ' Click For more details profile',
                          style: TextStyle(
                              fontSize: 9, fontWeight: FontWeight.bold),
                        ),
                      )
                    ],
                  ),
                  width: 150,
                  height: 150,
                ),
              ),
            ],
          )
        ],
      ),
    ));
  }
}
