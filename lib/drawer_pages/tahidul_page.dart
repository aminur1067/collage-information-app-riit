import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class TahidulPage extends StatefulWidget {
  const TahidulPage({Key? key}) : super(key: key);

  @override
  State<TahidulPage> createState() => _TahidulPageState();
}

class _TahidulPageState extends State<TahidulPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(

          body: Stack(

            children: [
              Container(
                width:400,height:200,child: Image.network('https://www.herzing.edu/sites/default/files/styles/fp_960_480/public/2020-09/how-to-become-software-engineer.jpg.webp?itok=uuamJN8l',fit: BoxFit.cover,),),
              Column(

                children: [
                  Padding(padding: EdgeInsets.only(top: 20)),
                  Row(
                    children: [
                      Padding(padding: EdgeInsets.only(left: 90)),
                      Center(
                        child: CircleAvatar(
                          radius: 100,
                          child: ClipOval(
                            child: Image.network(
                               'https://scontent.fdac96-1.fna.fbcdn.net/v/t1.6435-9/117769170_867896973736212_2949924993613681954_n.jpg?_nc_cat=106&ccb=1-7&_nc_sid=174925&_nc_eui2=AeHXrSR1Qbwez7nNge9lz88KJWJt79uvrRglYm3v26-tGLNesImDwDl-CCsoQVTHgjAp9duvdMDRUPP6RLxGe87p&_nc_ohc=edvTFXsx_B0AX_LYMJh&_nc_ht=scontent.fdac96-1.fna&oh=00_AfDXpA0WjxtAGUeBj2gDv6NhnisX0gKSkAJv4k0X1YXlZw&oe=6406B31F',fit: BoxFit.cover,
                              width: 200,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Column(
                    children: [
                      Row(
                        children: [
                          Padding(padding: EdgeInsets.only(left: 126)),
                          InkWell(
                              onTap: ()async {
                                final url="https://web.facebook.com/aminur.att/";

                                if(await canLaunch(url)){
                                  await launch(
                                      url,forceWebView: true,
                                      enableJavaScript: true
                                  );
                                }
                              },
                              child: Center(
                                  child: Image.network(
                                    'https://scontent.fdac96-1.fna.fbcdn.net/v/t39.30808-1/278001457_5441542185876354_710855040774611183_n.jpg?stp=dst-jpg_p320x320&_nc_cat=1&ccb=1-7&_nc_sid=1eb0c7&_nc_eui2=AeFtf94uRQNuQ_W4vyz1Y0Z3ZnI4iqGH40hmcjiKoYfjSIA-hsGSqMdYEqm8IUWZ8WzQ8693iZUIzUl8skMTzDuR&_nc_ohc=4OdPyys2aFcAX9k7eFr&_nc_ht=scontent.fdac96-1.fna&oh=00_AfBXo4_CBGYkDo8GKf2n-XEk9Ctuf6CWHaq6W9HTumtdsA&oe=63E4191F',
                                    height: 15,
                                  )
                              )
                          ),
                          SizedBox(width: 20,),
                          InkWell(
                              onTap: () {},
                              child: Center(
                                  child: Image.network(
                                    'https://scontent.fdac96-1.fna.fbcdn.net/v/t39.30808-6/281523213_5154082218010914_1249949579548042028_n.jpg?_nc_cat=1&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeH4R00FCuXjIvDUwyEGYB3cXC4en6wKYk9cLh6frApiT7YeFdVJFq27S2un0YR8ArOkd1iWbU_rimXD3OW0c5-G&_nc_ohc=ci55Co5NAnkAX88Eyfh&tn=2X1ylvy7n-bgOQX1&_nc_zt=23&_nc_ht=scontent.fdac96-1.fna&oh=00_AfDf9ZHzOhJI_q4ZtWcg99Xw3VnKR74Mo718DCH_NADPSg&oe=63E7746B',height: 20,
                                  )
                              )
                          ),
                          SizedBox(width: 20,),
                          InkWell(
                              onTap: () {},
                              child: Center(
                                  child: Image.network(
                                    'https://scontent.fdac96-1.fna.fbcdn.net/v/t1.18169-1/21751306_10155724905022838_7192191338970086519_n.png?stp=dst-png_p320x320&_nc_cat=1&ccb=1-7&_nc_sid=1eb0c7&_nc_eui2=AeHv4eCp5GyvP9Vkx-kjH6rQKKdkX6Xnl3Aop2RfpeeXcMdaPGtj6Ix5S8Kft1VYNLbSTYqNdpY2xXiyKaXYZ9oa&_nc_ohc=J9yhmZXp3WwAX_N6OmS&_nc_ht=scontent.fdac96-1.fna&oh=00_AfB22mjQOI6jmC5VPRdWAeYndW3H_kPyF6du1Ca7bLU2jw&oe=6405B18D',
                                    height: 15,
                                  )
                              )
                          ),
                          SizedBox(width: 20,),
                          InkWell(
                              onTap: () {},
                              child: Center(
                                  child: Image.network(
                                    'https://scontent.fdac96-1.fna.fbcdn.net/v/t1.6435-9/122142909_3581098625282670_771789260673887617_n.png?_nc_cat=1&ccb=1-7&_nc_sid=09cbfe&_nc_eui2=AeE2l131uFAmi1AHJCB1PE_sDQrW957a0cYNCtb3ntrRxkL8sDkY41inogGEBi4lV5PIuC21W4ILWPMJkbOP78L8&_nc_ohc=JGG1O1PnneYAX9ObZmW&_nc_ht=scontent.fdac96-1.fna&oh=00_AfCuRp4XePhWcDBrn-bZOnLOlx2BezX7t2DD5SPdCK8HPQ&oe=640AA7FF', height: 15,
                                  )
                              )
                          ),


                        ],
                      ),
                      SizedBox(height: 25,),
                      Row(
                        children: [
                          Padding(padding: EdgeInsets.only(left: 115)),
                          Center(child: Text('Flutter developer',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20),))
                        ],
                      ),
                      SizedBox(height: 10,),
                      Row(
                        children: [
                          Padding(padding: EdgeInsets.only(left: 120)),
                          Center(child: Text('MD TAUHIDUL ISLAM',style: TextStyle(fontWeight: FontWeight.bold,color: Colors.deepPurple),))
                        ],
                      ),
                      SizedBox(height: 87,),
                      Row(children: [
                        Padding(padding: EdgeInsets.all(20)),
                        Container(height:180,width:300,child: Text('Experienced software engineer with a     passion  for  developing   innovative   programs  that expedite the efficiency and effectiveness of organizational success. highlight its core competencies, and further its success.'))
                      ],)
                    ],
                  )
                ],
              ),],
          ),
        ));
  }
}
